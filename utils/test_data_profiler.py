import unittest
import flask_dataprofiler

class TestCalc(unittest.TestCase):
    def test_input1(self):
        self.assertRaises(TypeError, flask_dataprofiler.create_job, "data")

    def test_input3(self):
        self.assertRaises(TypeError, flask_dataprofiler.create_job)

    def test_input4(self):
        self.assertRaises(TypeError, flask_dataprofiler.status_pol, "data")

    def test_input6(self):
        self.assertRaises(TypeError, flask_dataprofiler.status_pol, )


if __name__ == '__main__':
    unittest.main()
~
