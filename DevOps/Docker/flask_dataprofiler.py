from flask import Flask, request, json
import boto3
import time
import common_constant

def create_job(name, path):
    job_name = name
    input_path = path
    # print(job_name)
    # print(input_path)
    client = boto3.client(service_name=common_constant.service_name, region_name=common_constant.region_name,
                          endpoint_url=common_constant.endpoint_url)
    response = client.start_job_run(JobName=job_name, Arguments={'--s3_path': input_path})
    # print("Job run id is" + str(response['JobRunId']))
    job_id = str(response['JobRunId'])
    return "Job Running, Job id is " + job_id


def status_pol(name, job_id):
    job_name = name
    # print(job_name)
    # print(job_id)
    client = boto3.client(service_name=common_constant.service_name, region_name=common_constant.region_name,
                          endpoint_url=common_constant.endpoint_url)
    status = client.get_job_run(JobName=job_name, RunId=job_id)
    # print("Job Status i s " + status['JobRun']['JobRunState'])
    if status:
        state = status['JobRun']['JobRunState']
        while state not in ['SUCCEEDED']:
            time.sleep(5)
            status = client.get_job_run(JobName=job_name, RunId=job_id)
            state = status['JobRun']['JobRunState']
            if state in ['STOPPED', 'FAILED', 'TIMEOUT']:
                return 'Failed to execute glue job: ' + status['JobRun']['ErrorMessage'] + '. State is : ' + state
            else :
                return 'IN PROGRESS'
        if state in ['SUCCEEDED']:
            print("")
            return "SUCCESS S3location s3://aws-a0049-use1-00-d-s3b-bpod-bdp-data01/processed/apps/profiler/outputs/dmp"


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def hello():
    return 'Hello World from Python Flask!'


@app.route('/Create_Job', methods=['GET', 'POST'])
def call_job():
    req_data = request.get_json()
    job_name = req_data['job_name']
    input_path = req_data['input_path']
    varr= create_job(job_name, input_path)
    return varr


@app.route('/Get_Status', methods=['GET', 'POST'])
def call_status_job():
    req_data = request.get_json()
    job_name = req_data['job_name']
    job_id = req_data['job_id']
    var = status_pol(job_name, job_id)
    return var


if __name__ == '__main__':
    app.run(host ='0.0.0.0',debug=True,port=5001)


